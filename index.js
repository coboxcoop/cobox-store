const crypto = require('hypercore-crypto')
const path = require('path')
const thunky = require('thunky')
const debug = require('debug')('cobox-group-store')
const maybe = require('call-me-maybe')
const { EventEmitter } = require('events')

const { uniq, isString } = require('./util')

module.exports = (storage, factory, opts) => new SpaceStore(storage, factory, opts)

class NotFoundError extends Error {
  constructor (message) {
    super(message)
    this.name = this.constructor.name
    Error.captureStackTrace(this, this.constructor);
    this.notFound = true
  }
}

class FoundError extends Error {
  constructor (message) {
    super(message)
    this.name = this.constructor.name
    Error.captureStackTrace(this, this.constructor);
    this.found = true
  }
}

class SpaceStore extends EventEmitter {
  constructor (storage, factory, opts = {}) {
    super()
    this._id = crypto.randomBytes(2).toString('hex')
    this.storage = storage
    this.opts = opts
    this.factory = factory
    this.collection = {}
    this._onCreate = opts.onCreate || noop
    this._readyCallback = thunky(this._ready.bind(this))
    this._closeCallback = thunky(this._close.bind(this))
  }

  seed (collection = []) {
    collection.forEach((_opts, i) => {
      this._cache(this._build(_opts))
    })
  }

  ready (callback) {
    return maybe(callback, new Promise((resolve, reject) => {
      this._readyCallback((err) => {
        if (err) return reject(err)
        else resolve()
      })
    }))
  }

  close (callback) {
    return maybe(callback, new Promise((resolve, reject) => {
      this._closeCallback((err) => {
        if (err) return reject(err)
        else resolve()
      })
    }))
  }

  async all () {
    await this.ready()
    return uniq(Object.values(this.collection))
  }

  async where (params = {}) {
    var response = await this.all()
    var keys = Object.keys(params)

    if (!keys.length) throw new Error('no parameters provided')

    for (var i = 0; i < keys.length; ++i) {
      var key = keys[i]

      response = response.filter((entry) => {
        if (!entry[key] || !params[key]) return false
        return entry[key].toString('hex') === params[key].toString('hex')
      })
    }

    if (!response.length) throw new NotFoundError('entry not found')

    return response
  }

  async findBy (params = {}) {
    let response = await this.where(params)
    return response[0]
  }

  async create (params = {}) {
    try {
      var entry = await this.findBy(params)
      if (entry) throw new FoundError('entry already exists')
    } catch (err) {
      if (!err.notFound) throw err
    }
    return await this.new(params)
  }

  new (params) {
    return new Promise((resolve, reject) => {
      var entry = this._build(params)

      entry.ready((err) => {
        if (err) return reject(err)
        this._cache(entry)
        this._onCreate(entry)
        this.emit('entry', entry)
        return resolve(entry)
      })
    })
  }

  // ------------------------------------------------------------------

  _ready (callback) {
    var pending = 1
    var collection = uniq(Object.values(this.collection))

    function next (n) {
      var entry = collection[n]
      if (!entry) return done()
      ++pending
      process.nextTick(next, n + 1)
      entry.ready(done)
    }

    function done (err) {
      if (err) {
        pending = Infinity
        return callback(err)
      }
      if (!--pending) {
        return callback()
      }
    }

    next(0)
  }

  _close (callback) {
    var pending = 1
    var collection = uniq(Object.values(this.collection))

    function next (n) {
      var entry = collection[n]
      if (!entry) return done()
      ++pending
      process.nextTick(next, n + 1)
      entry.close(done)
    }

    function done (err) {
      if (err) {
        pending = Infinity
        return callback(err)
      }
      if (!--pending) {
        return callback()
      }
    }

    next(0)
  }

  _build (params = {}) {
    return this.factory(
      this.storage,
      params.address,
      Object.assign(this.opts, params)
    )
  }

  _cache (entry) {
    this.collection[entry.address.toString('hex')] = entry
    if (entry.name) this.collection[entry.name] = entry
    return true
  }
}

function noop () {}
