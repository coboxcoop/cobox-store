const { describe } = require('tape-plus')
const Config = require('cobox-config')
const crypto = require('cobox-crypto')
const Space = require('cobox-group')
const Replicator = require('cobox-replicator')

const Store = require('../')
const { tmp, cleanup } = require('./util')

describe('space store: basic', (context) => {
  context('ready()', async function (assert, next) {
    let storage = tmp(),
      config = Config(storage),
      params = crypto.keySet()

    config.spaces.set(params.address, params)

    const collection = config.spaces.list()
    const store = Store(storage, Space, { config })
    store.seed(collection)

    await store.ready()
    assert.same(Object.values(store.collection).length, 1, 'builds then caches spaces from the config')
    assert.ok(store.collection[params.address.toString('hex')], 'accessible using address hex string')

    cleanup(storage, next)
  })

  context('all()', async function (assert, next) {
    let storage = tmp(),
      config = Config(storage),
      params = crypto.keySet()

    config.spaces.set(params.address, params)

    const collection = config.spaces.list()
    const store = Store(storage, Space, { config })
    store.seed(collection)

    await store.ready()
    var response = await store.all(params)
    assert.ok(Array.isArray(response), 'returns an array')
    assert.same(response.length, 1, 'returns the correct number')

    cleanup(storage, next)
  })

  context('where({ name, address })', async function (assert, next) {
    let storage = tmp(),
      config = Config(storage),
      params = Object.assign(crypto.keySet(), { name: 'magma' })

    seedConfig()
    config.spaces.set(params.address, params)

    const collection = config.spaces.list()
    const store = Store(storage, Space, { config })
    store.seed(collection)

    await store.ready()
    var response = await store.where({
      name: 'magma',
      address: params.address
    })

    assert.ok(Array.isArray(response), 'returns an array')
    var entry = response[0]
    assert.ok(entry, 'finds a entry')
    assert.same(entry && entry.address, params.address, 'finds the correct entry')

    cleanup(storage, next)

    function seedConfig () {
      var space1 = Object.assign(crypto.keySet(), { name: 'cobox' }),
        space2 = Object.assign(crypto.keySet(), { name: 'iuvia' }),
        space3 = Object.assign(crypto.keySet(), { name: 'blockades' })

      config.spaces.set(space1.address, space1)
      config.spaces.set(space2.address, space2)
      config.spaces.set(space3.address, space3)
    }
  })

  context('where({ name, address: null })', async function (assert, next) {
    let storage = tmp(),
      config = Config(storage),
      params = Object.assign(crypto.keySet(), { name: 'magma' })

    config.spaces.set(params.address, params)

    const collection = config.spaces.list()
    const store = Store(storage, Space, { config })
    store.seed(collection)

    try {
      await store.ready()

      var response = await store.where({
        name: 'magma',
        address: null
      })

      assert.same(response.length, 0, 'fails to find an entry')
    } catch (err) {
      assert.error(err, 'no error')
    }

    cleanup(storage, next)
  })

  context('where({ name: null, address })', async function (assert, next) {
    let storage = tmp(),
      config = Config(storage),
      params = Object.assign(crypto.keySet(), { name: 'magma' })

    config.spaces.set(params.address, params)

    const collection = config.spaces.list()
    const store = Store(storage, Space, { config })
    store.seed(collection)

    try {
      await store.ready()

      var response = await store.where({
        name: null,
        address: params.address
      })

      assert.same(response.length, 0, 'fails to find an entry')
    } catch (err) {
      assert.error(err, 'no error')
    }

    cleanup(storage, next)
  })

  context('where({ name, address: "wrong address" })', async function (assert, next) {
    let storage = tmp(),
      config = Config(storage),
      params = Object.assign(crypto.keySet(), { name: 'magma' })

    config.spaces.set(params.address, params)

    const collection = config.spaces.list()
    const store = Store(storage, Space, { config })
    store.seed(collection)

    try {
      await store.ready()

      var response = await store.where({
        name: 'magma',
        address: crypto.randomBytes(32).toString('hex')
      })

      assert.same(response.length, 0, 'fails to find an entry')
    } catch (err) {
      assert.error(err, 'no error')
    }

    cleanup(storage, next)
  })

  context('where({ name: "wrong name", address })', async function (assert, next) {
    let storage = tmp(),
      config = Config(storage),
      params = Object.assign(crypto.keySet(), { name: 'magma' })

    config.spaces.set(params.address, params)

    const collection = config.spaces.list()
    const store = Store(storage, Space, { config })
    store.seed(collection)

    try {
      await store.ready()

      var response = await store.where({
        name: 'cobox',
        address: params.address
      })

      assert.same(response.length, 0, 'fails to find an entry')
    } catch (err) {
      assert.error(err, 'no error')
    }

    cleanup(storage, next)
  })

  context('where({ name })', async function (assert, next) {
    let storage = tmp(),
      config = Config(storage),
      params = Object.assign(crypto.keySet(), { name: 'magma' })

    config.spaces.set(params.address, params)

    const collection = config.spaces.list()
    const store = Store(storage, Space, { config })
    store.seed(collection)

    try {
      await store.ready()

      var response = await store.where({ name: 'magma' })
      assert.ok(Array.isArray(response), 'returns an array')
      var space = response[0]
      assert.ok(space, 'finds a space')
      assert.same(space && space.address, params.address, 'finds the correct space')
    } catch (err) {
      assert.error(err, 'no error')
    }

    cleanup(storage, next)
  })

  context('where({ address })', async function (assert, next) {
    let storage = tmp(),
      config = Config(storage),
      params = crypto.keySet()

    config.spaces.set(params.address, params)

    const collection = config.spaces.list()
    const store = Store(storage, Space, { config })
    store.seed(collection)

    try {
      await store.ready()

      var response = await store.where({ address: params.address })
      assert.ok(Array.isArray(response), 'returns an array')
      var space = response[0]
      assert.ok(space, 'finds a space')
      assert.same(space && space.address, params.address, 'finds the correct space')
    } catch (err) {
      assert.error(err, 'no error')
    }

    cleanup(storage, next)
  })

  context('findBy()', async function (assert, next) {
    let storage = tmp(),
      config = Config(storage),
      params = crypto.keySet()

    config.spaces.set(params.address, params)

    const collection = config.spaces.list()
    const store = Store(storage, Space, { config })
    store.seed(collection)

    try {
      await store.ready()

      delete params.encryptionKey
      var response = await store.findBy(params)
      assert.ok(response, 'finds a space')
      assert.same(response && response.address, params.address, 'finds the correct space')
    } catch (err) {
      assert.error(err, 'no error')
    }
    cleanup(storage, next)
  })

  context('create()', async function (assert, next) {
    let pending = 2
    let storage = tmp(),
      config = Config(storage),
      params = Object.assign(crypto.keySet(), { name: 'magma' })

    const collection = config.spaces.list()
    const store = Store(storage, Space, { config })
    store.seed(collection)

    store.on('entry', (entry) => {
      assert.ok(entry.address, 'emits the entry')
      done()
    })

    var space = await store.create(params)
    assert.ok(space && space.address, 'creates a new space')
    done()

    function done () {
      if (--pending) return cleanup(storage, next)
    }
  })

  // context('destroy()', async function (assert, next) {
  //   let storage = tmp(),
  //     config = Config(storage),
  //     params = crypto.keySet()

  //   config.spaces.set(params.address, params)
  //   const store = Store(Space, storage, { config })

  //   var spaces = await store.all()
  //   console.log(spaces.map((space) => space.address.toString('hex')))
  //   var success = await store.destroy(params)

  //   assert.ok(success, 'deleted the space')
  //   cleanup(storage, next)
  // })
})

describe('multiple', (context) => {
  context('factory', async (assert, next) => {
    let storage = tmp(),
      config = Config(storage),
      space = crypto.keySet(),
      replicator = { address: crypto.address() }

    config.spaces.set(space.address, space)
    config.replicators.set(replicator.address, replicator)

    const spaces = config.spaces.list()
    const replicators = config.replicators.list()

    const api = {
      spaces: {
        store: Store(storage, Space, { config })
      },
      replicators: {
        store: Store(storage, Replicator, { config })
      }
    }

    api.spaces.store.seed(spaces)
    api.replicators.store.seed(replicators)

    await api.spaces.store.ready()
    await api.replicators.store.ready()

    assert.same(Object.values(api.spaces.store.collection).length, 1, 'builds then caches spaces')
    assert.same(Object.values(api.replicators.store.collection).length, 1, 'builds then caches replicators')

    assert.ok(api.spaces.store.collection[space.address.toString('hex')], 'accessible using address hex string')
    assert.ok(api.replicators.store.collection[replicator.address.toString('hex')], 'accessible using address hex string')

    cleanup(storage, next)
  })
})
