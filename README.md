# cobox-group-store

A repository for your cobox-groups. Adhere's to a standard CRUD repository pattern.

## API

```js
const Store = require('cobox-group-store')
const storage = 'path/to/storage' // defaults to storage path defined in cobox-constants package
const store = Store(storage)

let spaces = await store.all() // ensures spaces loaded, then returns an array of cobox-groups

await store.findBy({ name, address })
await store.where({ name, address })
await store.create({ name, address })
```
